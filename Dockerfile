FROM node:10 as builder

ADD package.json package.json
ADD package-lock.json package-lock.json
RUN npm install --only=prod


FROM node:10-alpine
LABEL version="0.1.0"

WORKDIR /usr/src/app
COPY --from=builder node_modules node_modules
ADD . .

RUN ls .

EXPOSE 1338

CMD node app.js --prod
