/**
 * Trade.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    name: { type: 'string', required: true, },
    tradeType: { type: 'string', required: true,  unique: true },
    tradePhoneNumber: { type: 'string', required: true,  unique: true },
    password: { type: 'string', required: true, },

    avatarUrl: { type: 'string',  },
    avatarName: { type: 'string',  },
    avatarFd: { type: 'string',  },

    code: { type: 'string',  },

    traderFirstName: { type: 'string',  },
    traderLastName: { type: 'string',},
    traderPhoneNumber: { type: 'string',},


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    favoriteMeals: {
      collection: 'favoritemeal',
      via: 'trade'
    },

    menus: {
      collection: 'menu',
      via: 'trade'
    }

  },
/*
  customToJSON: function() {
    // Return a shallow copy of this record with the password and ssn removed.
    return _.omit(this, ['tradeType' ])
  },*/

};

