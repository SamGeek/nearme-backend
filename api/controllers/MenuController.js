/**
 * MenuController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


  saveMenu: async function (req, res) {

    var menuCreated = await Menu.create({
      name: req.allParams().name,
      price: req.allParams().price,
      category: req.allParams().category,

      trade: req.allParams().tradeID,
    }).fetch();

    return res.ok(menuCreated);
  },



  //service to fetch menus by trade ID
  fetchMenus: async function (req, res){

    var menus =  await Menu.find({trade: req.allParams().tradeID,});

    return res.ok(menus)
  },


};

