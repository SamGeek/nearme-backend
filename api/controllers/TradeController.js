/**
 * TradeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const fs = require('fs')
const path = require('path')

const accountSid = 'AC93e324b2f68d71b18b8056b92f163b58'
const authToken =  sails.config.custom.twilioAuthToken

module.exports = {

  //write the method to upload the picture
  // and to save other data
  /**
   * Upload avatar for currently logged-in user
   *
   * (POST /user/avatar)
   */
  saveUserWithAvatar: async function (req, res) {

    // sails.log('requete 1 : ',req.allParams().data)
    // sails.log('requete 1 : '+req.data.traderPhoneNumber)
    // const data = JSON.parse(req.allParams().data)

    var tradeRecord = await Trade.findOne({
      traderPhoneNumber: req.allParams().traderPhoneNumber,
    });

    sails.log(tradeRecord);

    // If there was no matching user, respond thru the "badCombo" exit.
    if(tradeRecord  !== undefined) {
      return  res.notFound();
    }

    var tradeCreated = await Trade.create({
      name: req.allParams().name,
      tradeType: req.allParams().tradeType,
      tradePhoneNumber: req.allParams().tradePhoneNumber,
      password: req.allParams().password,

      traderFirstName: req.allParams().traderFirstName,
      traderLastName: req.allParams().traderLastName,
      traderPhoneNumber: req.allParams().traderPhoneNumber,
    }).fetch();

    const uploadDir = path.resolve(
      sails.config.appPath,
      sails.config.globals.uploadDirs['default']
    );

    if (!fs.existsSync(uploadDir)){
      fs.mkdirSync(uploadDir)
    }

    req.file('avatar').upload({
      dirname: uploadDir,
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    },async function whenDone(err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0){
        return res.badRequest('No file was uploaded');
      }

      // Get the base URL for our deployed application from our custom config
      // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
      var baseUrl = sails.config.custom.baseUrl;
      var code = Math.floor(1000 + Math.random() * 9000);

      // Save the "fd" and the url where the avatar for a user can be accessed
      var updatedTrade =  await Trade.update( {  id: tradeCreated.id }).set({
        code:code,
        // Generate a unique URL where the avatar can be downloaded.
        avatarUrl: require('util').format('%s/trade/avatar/%s', baseUrl, tradeCreated.id),
        avatarName: req.file('avatar')._files[0].stream.filename,

        // Grab the first file and use it's `fd` (file descriptor)
        avatarFd: uploadedFiles[0].fd
      }).fetch();

      //now that everything is working we can send the sms
      // require the Twilio module and create a REST client
      const client = require('twilio')(accountSid, authToken)

      client.messages.create(
        {
          to: req.allParams().traderPhoneNumber,
          from: '+16466667887',
          body: 'Votre code de connexion est : '+code,
        },
        (err, message) => {
          if (err) return res.serverError(err);
          console.log(message.sid);
          return res.ok(updatedTrade);
        }
      )

      return res.ok(updatedTrade[0]);

    });
  },


  /**
   * Download avatar of the user with the specified id
   *
   * (GET /user/avatar/:id)
   */
  avatar: function (req, res){

    Trade.findOne(req.param('id')).exec(function (err, trade){
      if (err) return res.serverError(err);
      if (!trade) return res.notFound();

      // Trade has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!trade.avatarFd) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // set the filename to the same file as the trade uploaded
      // res.set("Content-disposition", "attachment; filename='" + file.name + "'");

      // Stream the file down
      fileAdapter.read(trade.avatarFd)
        .on('error', function (err){
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

  //service to verify trader phoneNumber
  sendOTPtoTraderNumber: function (req, res){

    Trade.findOne( {traderPhoneNumber: req.allParams().traderPhoneNumber,}  ).exec( async function (err, trade){
      if (err) return res.serverError(err);
      if (!trade) return res.notFound();

      var code = Math.floor(1000 + Math.random() * 9000);

      trade.code =  code
      var tradeUpdated = await Trade.updateOne({ id: trade.id }).set(trade);

      // require the Twilio module and create a REST client
      const client = require('twilio')(accountSid, authToken)

      client.messages.create(
        {
          to: trade.traderPhoneNumber,
          from: '+16466667887',
          body: 'Votre code de connexion est : '+code,
        },
        (err, message) => {
          if (err) return res.serverError(err);
          console.log(message.sid);
          return res.ok();
        }
      )

    });
  },


  //service to verify trader phoneNumber
  verifyTraderNumber: async function (req, res){

    var trade =  await Trade.findOne({traderPhoneNumber: req.allParams().traderPhoneNumber,});

    if (!trade) return res.notFound();

    if (trade.code !== req.allParams().userCode ) {
      return res.forbidden();
    }

    return res.ok(trade)
  },


};

