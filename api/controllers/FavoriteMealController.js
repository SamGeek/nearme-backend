/**
 * FavoriteMealController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {


  saveFavoriteMeal: async function (req, res) {

    sails.log('photo object',req.allParams().photo)

    var fmCreated = await FavoriteMeal.create({
      name: req.allParams().name,
      price: req.allParams().price,

      trade: req.allParams().tradeID,
      photo: req.allParams().photo,
    }).fetch();

    var photoObject =  await Photo.find({id: req.allParams().photo});

    fmCreated.photo = photoObject[0]

    return res.ok(fmCreated);
  },


  //service to fetch favorite meal by trade ID
  fetchFavoriteMeals: async function (req, res){

    var favoriteMeals =  await FavoriteMeal.find({trade: req.allParams().tradeID,}).populate('photo');

    return res.ok(favoriteMeals)
  },


};

