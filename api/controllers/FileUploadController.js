/**
 * FileUploadController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const fs = require('fs')
const path = require('path')

module.exports = {

  uploadPhoto: async function (req, res) {

    const uploadDir = path.resolve(
      sails.config.appPath,
      sails.config.globals.uploadDirs['default']
    );

    if (!fs.existsSync(uploadDir)){
      fs.mkdirSync(uploadDir)
    }

    req.file('photo').upload({
      dirname: uploadDir,
      // don't allow the total upload size to exceed ~10MB
      // to this verification on mobile part
      maxBytes: 10000000
    },async function whenDone(err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0){
        return res.badRequest('No file was uploaded');
      }

      // Get the base URL for our deployed application from our custom config
      // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
      var baseUrl = sails.config.custom.baseUrl;

      // Save the "fd" and the url where the avatar for a user can be accessed
      var createdPhoto =  await Photo.create( {
        // Generate a unique URL where the avatar can be downloaded.
        photoUrl: "",
        photoName: req.file('photo')._files[0].stream.filename,
        // Grab the first file and use it's `fd` (file descriptor)
        photoFd: uploadedFiles[0].fd
      }).fetch();

      sails.log('photo cree  ',createdPhoto)

      var updatedPhoto =  await Photo.update( {  id: createdPhoto.id }).set({
        photoUrl: require('util').format('%s/photo/download/%s', baseUrl, createdPhoto.id),
      }).fetch();

      sails.log('photo mise a jour avec le lien  ',updatedPhoto)

      return res.ok(updatedPhoto[0]);

    });
  },


  /**
   * Download photo of the user with the specified id
   *
   * (GET /photo/download/:id)
   */
  downloadPhoto: function (req, res){

    Photo.findOne(req.param('id')).exec(function (err, photo){
      if (err) return res.serverError(err);
      if (!photo) return res.notFound();

      // photo has no photo image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!photo.photoFd) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // set the filename to the same file as the photo uploaded
      // res.set("Content-disposition", "attachment; filename='" + file.name + "'");

      // Stream the file down
      fileAdapter.read(photo.photoFd)
        .on('error', function (err){
          return res.serverError(err);
        })
        .pipe(res);
    });
  },

};

